FROM porchn/php5.6-apache

COPY archive-sources.list /etc/apt/sources.list

RUN apt-get -y update \
    && apt-get install libldb-dev libldap2-dev ssmtp mailutils -y --force-yes \
    && ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so \
    && docker-php-ext-install ldap \
    && echo "sendmail_path= /usr/sbin/sendmail -t -i" >> /usr/local/etc/php/php.ini \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*
